import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

class SettingsScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', height: 150, width: '100%' }}>
                    <TouchableOpacity style={styles.item}>
                        <View style={styles.itemContent}>
                            <Text style={styles.itemIcon}>
                                <FontAwesome>{Icons.key}</FontAwesome>
                            </Text>
                            <Text style={styles.itemText}>Study rooms</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                        <View style={styles.itemContent}>
                            <Text style={styles.itemIcon}>
                                <FontAwesome>{Icons.fileTextO}</FontAwesome>
                            </Text>
                            <Text style={styles.itemText}>Documents</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <TouchableOpacity style={styles.item}>
                        <View style={styles.itemContent}>
                            <Text style={styles.itemIcon}>
                                <FontAwesome>{Icons.terminal}</FontAwesome>
                            </Text>
                            <Text style={styles.itemText}>Events</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                        <View style={styles.itemContent}>
                            <Text style={styles.itemIcon}>
                                <FontAwesome>{Icons.graduationCap}</FontAwesome>
                            </Text>
                            <Text style={styles.itemText}>Grades</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.row}>
                    <TouchableOpacity style={styles.item}>
                        <View style={styles.itemContent}>
                            <Text style={styles.itemIcon}>
                                <FontAwesome>{Icons.coffee}</FontAwesome>
                            </Text>
                            <Text style={styles.itemText}>Services</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.item}>
                        <View style={styles.itemContent}>
                            <Text style={styles.itemIcon}>
                                <FontAwesome>{Icons.question}</FontAwesome>
                            </Text>
                            <Text style={styles.itemText}>Help</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 50,
        paddingHorizontal: 20,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    row: {
        flexDirection: 'row',
        height: 150,
        width: '100%'
    },
    item: {
        flex: 1,
        margin: 5,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#007bc0',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        shadowColor: '#8c8c8c',
        shadowOffset: { x: 0, y: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 5
    },
    itemContent: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemIcon: {
        color: '#007bc0',
        fontSize: 45
    },
    itemText: {
        color: '#007bc0',
        paddingTop: 10,
        fontSize: 18
    }
});

export { SettingsScreen };
