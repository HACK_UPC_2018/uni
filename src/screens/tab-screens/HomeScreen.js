import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';

class HomeScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.component, styles.componentFullWidth]}>
                    <Text>
                        <FontAwesome>{Icons.strikethrough}</FontAwesome>
                    </Text>
                </View>
                <View style={{ flexDirection: 'row', width: '85%', height: 150 }}>
                    <View style={[styles.component, { flex: 1 }]}>
                        <Text>
                            <FontAwesome>{Icons.strikethrough}</FontAwesome>
                        </Text>
                    </View>
                    <View style={[styles.component, { flex: 1 }]}>
                        <Text>
                            <FontAwesome>{Icons.strikethrough}</FontAwesome>
                        </Text>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 10, width: '85%', height: 150 }}>
                    <View style={[styles.component, { flex: 2 }]}>
                        <Text>
                            <FontAwesome>{Icons.strikethrough}</FontAwesome>
                        </Text>
                    </View>
                    <View style={[styles.component, { flex: 1 }]}>
                        <Text>
                            <FontAwesome>{Icons.strikethrough}</FontAwesome>
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={{
                        width: 60,
                        height: 60,
                        borderColor: '#007bc0',
                        borderRadius: 30,
                        borderWidth: 2,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 20,
                        shadowColor: '#8c8c8c',
                        shadowOffset: { x: 0, y: 0 },
                        shadowOpacity: 0.5,
                        shadowRadius: 5
                    }}
                >
                    <Text style={{ color: '#007bc0', fontWeight: 'bold' }}>Edit</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 50,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    component: {
        height: 150,
        margin: 5,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#007bc0',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        shadowColor: '#8c8c8c',
        shadowOffset: { x: 0, y: 0 },
        shadowOpacity: 0.3,
        shadowRadius: 5
    },
    componentFullWidth: {
        width: '85%'
    }
});

export { HomeScreen };
