import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, Image } from 'react-native';
import _ from 'lodash';
import manager from '../../lib/manager';

import userStore from '../../stores/UserStore';

class ProfileScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nom: '',
            cognoms: '',
            correu: ''
        };
    }

    componentDidMount() {
        manager.getMe(info => {
            this.setState({
                nom: info.nom,
                cognoms: info.cognoms,
                correu: info.email
            });
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={{ width: 50, height: 50 }}
                    source={{
                        uri: 'https://api.fib.upc.edu/v2/jo/foto.jpg',
                        headers: { Authorization: `Bearer ${userStore.accessToken}` }
                    }}
                />
                <FlatList
                    data={[{ value: this.state.nom }, { value: this.state.cognoms }, { value: this.state.correu }]}
                    renderItem={({ item, data }) => {
                        return (
                            <View style={styles.container}>
                                <View style={{ width: '100%' }}>
                                    <Text style={styles.item}>{item.value}</Text>
                                </View>
                            </View>
                        );
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 50
    },
    item: {
        width: '100%',
        borderWidth: 1,
        // backgroundColor: 'green',
        borderColor: '#8c8c8c',
        borderRadius: 15,
        padding: 10,
        marginVertical: 10,
        marginTop: 30
    },
    img: {
        marginTop: 100
    }
});

export { ProfileScreen };
