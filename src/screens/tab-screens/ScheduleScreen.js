import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, FlatList, StatusBar } from 'react-native';
import CalendarStrip from 'react-native-calendar-strip';
import moment from 'moment';
import _ from 'lodash';
import manager from '../../lib/manager';

class ScheduleScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            day: moment().startOf('isoweek')
        };
    }

    prepareHorari = () => {
        manager.getHorari(horari => {
            this.setState({ data: _.filter(horari, { dia_setmana: this.state.day }) });
        });
    };

    componentDidMount() {
        this.prepareHorari();
        console.log('');
    }
    render() {
        console.log('DOIES', this.state.data);
        return (
            <View style={styles.calendar}>
                <StatusBar translucent={false} backgroundColor="rgb(255, 255, 255)" />
                <CalendarStrip
                    ref={ref => {
                        this.ref = ref;
                    }}
                    calendarAnimation={{ type: 'sequence', duration: 30 }}
                    daySelectionAnimation={{
                        type: 'border',
                        duration: 200,
                        borderWidth: 1,
                        borderHighlightColor: 'white'
                    }}
                    onDateSelected={date => {
                        this.setState({ day: moment(date).isoWeekday() });
                        this.prepareHorari();
                    }}
                    style={{ height: 100, paddingTop: 20, paddingHorizontal: 20, paddingBottom: 10 }}
                    dateNameStyle={{ color: 'white' }}
                    highlightDateNumberStyle={{ color: '#007bc0' }}
                    highlightDateNameStyle={{ color: '#007bc0' }}
                    disabledDateNameStyle={{ color: 'grey' }}
                    disabledDateNumberStyle={{ color: 'grey' }}
                />
                <FlatList
                    style={{ flex: 1, width: '100%' }}
                    contentContainerStyle={styles.container2}
                    data={this.state.data}
                    ListEmptyComponent={
                        <Text
                            style={{
                                width: '100%',
                                height: '100%',
                                textAlign: 'center',
                                textAlignVertical: 'center',
                                marginTop: 100,
                                fontSize: 30
                            }}
                        >
                            No hi ha cap clase :)
                        </Text>
                    }
                    renderItem={({ item, data }) => {
                        console.log(item);
                        return (
                            <View style={styles.item}>
                                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ flex: 1 }}>
                                        {item.inici}-{Number(item.inici[0]) * 10 + Number(item.inici[1]) + item.durada}:
                                        {`${item.inici[3]}${item.inici[4]}`}
                                    </Text>
                                    <Text style={{ flex: 1, textAlign: 'center' }}>{item.codi_assig}</Text>
                                    <Text style={{ flex: 1, textAlign: 'right' }}>{item.aules}</Text>
                                </View>
                            </View>
                        );
                    }}
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        //alignItems: 'center',
        height: '100%',
        width: '100%',
        marginTop: 100
    },
    container2: {
        flexGrow: 1,
        width: '100%',
        alignItems: 'center',
        paddingHorizontal: 10
    },
    item: {
        width: '100%',
        borderWidth: 1,
        borderColor: '#8c8c8c',
        borderRadius: 15,
        padding: 10,
        marginVertical: 10,
        flexDirection: 'row'
    },
    calendar: {
        backgroundColor: 'white',
        marginTop: 20,
        flex: 1,
        width: '100%',
        height: '100%'
        //alignItems: 'center',
        // justifyContent: 'center'
        //backgroundColor: 'blue'
    }
});

export { ScheduleScreen };
