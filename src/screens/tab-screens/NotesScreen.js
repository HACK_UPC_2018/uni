import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    SectionList,
    TouchableOpacity,
    TouchableWithoutFeedback,
    WebView,
    Modal,
    Dimensions
} from 'react-native';
import manager from '../../lib/manager';
import _ from 'lodash';
import { decorate, observable } from 'mobx';
import moment from 'moment';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import Collapsible from 'react-native-collapsible';
import { COLORS } from '../../lib/conf';

class NotesScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notes: [],
            opened: {
                id: -1,
                section: ''
            }
        };
    }

    componentDidMount() {
        console.log('LOADING NOTES');
        manager.getNotes(notes => {
            console.log('NOTES AUX-PRE', notes);
            let notes_aux = _.groupBy(notes, 'codi_assig');
            console.log('NOTES AUX', notes_aux);
            let notes_final = [];
            for (var property in notes_aux) {
                if (notes_aux.hasOwnProperty(property)) {
                    notes_final.push({ title: property, data: notes_aux[property] });
                }
            }
            console.log('FINAL NOTES', notes_final);
            this.setState({ notes: notes_final });
        });
    }

    shouldComponentUpdate() {
        return true;
    }

    render() {
        return (
            <View style={styles.container}>
                <SectionList
                    style={styles.list}
                    sections={this.state.notes}
                    renderItem={({ item, index, section }) => {
                        console.log('INDEX', index, section);

                        return (
                            <View style={styles.item}>
                                <TouchableOpacity
                                    onPress={() => {
                                        if (
                                            this.state.opened.id === index &&
                                            this.state.opened.section === section.title
                                        ) {
                                            this.setState({ opened: { id: -1, section: '' } });
                                        } else {
                                            this.setState({ opened: { id: index, section: section.title } });
                                        }
                                    }}
                                    style={{ flexDirection: 'row', width: '100%' }}
                                >
                                    <View>
                                        <Text style={styles.data}>{moment(item.data_insercio).format('DD/MM/YY')}</Text>
                                        <Text style={styles.data}>{moment(item.data_insercio).format('HH:MM')}</Text>
                                    </View>
                                    <View
                                        style={{
                                            flex: 1,
                                            width: '100%',
                                            flexDirection: 'row',
                                            justifyContent: 'space-between',
                                            alignItems: 'center'
                                        }}
                                    >
                                        <Text style={{ paddingRight: 20 }}>
                                            {item.adjunts.length > 0 && (
                                                <Text style={{ marginRight: 10 }}>
                                                    <FontAwesome>{Icons.paperclip}</FontAwesome>
                                                    {'  '}
                                                </Text>
                                            )}
                                            {item.titol}
                                        </Text>
                                        <Text>
                                            {!(
                                                this.state.opened.id === index &&
                                                this.state.opened.section === section.title
                                            ) && <FontAwesome>{Icons.angleDown}</FontAwesome>}
                                            {this.state.opened.id === index &&
                                                this.state.opened.section === section.title && (
                                                    <FontAwesome>{Icons.angleUp}</FontAwesome>
                                                )}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <Collapsible
                                    style={{ paddingTop: 5, flexDirection: 'row', justifyContent: 'center' }}
                                    collapsed={
                                        !(this.state.opened.id === index && this.state.opened.section === section.title)
                                    }
                                >
                                    {/* <WebView
                                        style={{ height: 100 }}
                                        scalesPageToFit={true}
                                        automaticallyAdjustContentInsets={true}
                                        source={{ html: item.text }}
                                    /> */}
                                    <TouchableOpacity onPress={() => {}} style={styles.noteButton}>
                                        <Text style={styles.noteButtonText}>
                                            <FontAwesome>{Icons.search}</FontAwesome>
                                        </Text>
                                    </TouchableOpacity>
                                    {item.adjunts.length > 0 && (
                                        <TouchableOpacity onPress={() => {}} style={styles.noteButton}>
                                            <Text style={styles.noteButtonText}>
                                                <FontAwesome>{Icons.download}</FontAwesome>
                                            </Text>
                                        </TouchableOpacity>
                                    )}
                                </Collapsible>
                            </View>
                        );
                    }}
                    SectionSeparatorComponent={({ section }) => (
                        <View style={{ height: 2, backgroundColor: '#8c8c8c', width: '100%' }} />
                    )}
                    ItemSeparatorComponent={({ section }) => (
                        <View style={{ height: 2, backgroundColor: '#8c8c8c30', width: '100%' }} />
                    )}
                    renderSectionHeader={({ section }) => {
                        console.log('SECTION', section);

                        return (
                            <View style={styles.sectionContainer}>
                                <Text style={styles.section}>{section.title}</Text>
                            </View>
                        );
                    }}
                    ListEmptyComponent={<Text>Loading notes...</Text>}
                />
            </View>
        );
    }
}

decorate(NotesScreen, {
    notes: observable
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 30,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    list: {
        width: '100%'
    },
    item: {
        width: '100%',
        backgroundColor: 'white',
        paddingVertical: 10,
        paddingHorizontal: 20
    },
    data: {
        width: 60,
        marginRight: 10,
        fontSize: 12,
        color: '#8c8c8c'
    },
    section: {
        color: '#ffffff',
        fontSize: 20,
        fontWeight: 'bold',
        textTransform: 'uppercase'
    },
    sectionContainer: {
        backgroundColor: '#007bc0',
        paddingVertical: 5,
        paddingLeft: 20
    },
    noteButton: {
        marginHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        height: 45,
        width: 45,
        borderRadius: 23,
        borderWidth: 1,
        borderColor: '#007bc0'
    },
    noteButtonText: {
        fontSize: 23,
        color: '#007bc0',
        textAlign: 'center'
    }
});

export { NotesScreen };
