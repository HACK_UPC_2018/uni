import React, { Component } from 'react';
import { StyleSheet, WebView, Linking } from 'react-native';

import userStore from '../../stores/UserStore';

type Props = {};
export default class LoginScreen extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            bodyHTML: undefined,
            loading: false
        };
    }

    componentWillUnmount() {
        Linking.removeEventListener('url', this._handleOpenURL);
    }

    getParams = url => {
        var urlParams = {};
        var keyValuePairs = url.split('&');
        for (var i = 0; i < keyValuePairs.length; i++) {
            var keyValuePair = keyValuePairs[i].split('=');
            var paramName = keyValuePair[0];
            var paramValue = keyValuePair[1] || '';
            urlParams[paramName] = decodeURIComponent(paramValue.replace(/\+/g, ' '));
        }
        return urlParams;
    };

    _handleOpenURL = async event => {
        var params = this.getParams(event.url);
        console.log('EVENT', params['code']);
        //userStore.accessToken = params['code'];
        const body = `grant_type=authorization_code&redirect_uri=apifib://UNi&code=${
            params['code']
        }&client_id=WkSV5szIxgQSSkBcA9PjFgcEK5HPRtVZo9EUF9rP&client_secret=W3CXvewHgFY22RcA5zPe1X2ogC5ku6UCuZzR51n3YE8L5fDxipDJeXfGKfl4bUxCvnQ1szL7U9HSGpJeMdAPzZnMXmsNn3F4G7pex8Q7ZeDhlUVoilDOJHE46rYWmwBS`;

        const response = await fetch('https://api.fib.upc.edu/v2/o/token', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body
        });
        console.log('TOKEN RESPONSE', response);
        const json = await response.json();
        console.log('TOKEN RESPONSE (JSON)', json);
        userStore.accessToken = json.access_token;
        userStore.accessTokenExpiry = json.expires_in;
        userStore.renewToken = json.refresh_token;
        console.log('USER', userStore.accessToken, userStore.accessTokenExpiry);

        this.props.navigation.navigate('tabScreens');
    };

    componentDidMount() {
        Linking.getInitialURL()
            .then(url => {
                if (url) {
                    console.log('Initial url is: ' + url);
                }
            })
            .catch(err => console.error('An error occurred', err));
        Linking.addEventListener('url', this._handleOpenURL);
    }

    render() {
        return (
            <WebView
                onLoad={data => {
                    console.log('ON LOAD', data);
                }}
                onLoadEnd={data => {
                    console.log('ON LOAD END', data);
                }}
                onMessae={data => {
                    console.log('ON MESSAGE', data);
                }}
                onNavigationStateChange={data => {
                    console.log('ON NAVIGATION STATE CHANGE', data);
                }}
                originWhitelist={['file://']}
                source={{
                    url:
                        'https://api.fib.upc.edu/v2/o/authorize/?client_id=WkSV5szIxgQSSkBcA9PjFgcEK5HPRtVZo9EUF9rP&redirect_uri=apifib://UNi&response_type=code&state=random_state_string'
                }}
            />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
