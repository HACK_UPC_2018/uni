import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

import { COLORS } from '../../lib/conf';
import userStore from '../../stores/UserStore';

type Props = {};
export default class WelcomeScreen extends Component<Props> {
    constructor(props) {
        super(props);
        this.state = {
            bodyHTML: undefined
        };
    }

    render() {
        if (!userStore.ready) {
            return null;
        }
        return (
            <View style={styles.container}>
                <Text style={styles.icon}>{'\ue900'}</Text>
                <TouchableOpacity
                    style={{
                        marginTop: 150,
                        paddingVertical: 7,
                        paddingHorizontal: 50,
                        backgroundColor: 'white',
                        borderRadius: 20
                    }}
                    onPress={() => {
                        this.props.navigation.navigate('login');
                    }}
                >
                    <Text style={{ color: '#007bc0', fontSize: 20, fontWeight: 'bold' }}>Log in</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.teal
    },
    icon: {
        color: '#fff',
        fontFamily: 'uni',
        fontSize: 200
    }
});
