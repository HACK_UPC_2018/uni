import React, { Component } from 'react';
import { Animated, Easing, StyleSheet, View, Keyboard } from 'react-native';
import PropTypes from 'prop-types';
import { ifIphoneX } from 'react-native-iphone-x-helper';

import { decorate, observable } from 'mobx';

import { tabsDef, COLORS } from '../lib/conf';
import { TabButton } from './TabButton';

type Props = {};
class TabBar extends Component<Props> {
    static propTypes = {
        navigation: PropTypes.object
    };

    bottomAnimation = new Animated.Value(1);

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    }
    componentWillUnmount() {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    keyboardDidShow = () => {
        this.bottomAnimation.setValue(0);
    };

    keyboardDidHide = () => {
        Animated.timing(this.bottomAnimation, {
            toValue: 1,
            duration: 400,
            easing: Easing.in,
            useNativeDriver: true
        }).start();
    };

    render() {
        const bottomPosition = this.bottomAnimation.interpolate({
            inputRange: [0, 1],
            outputRange: [100, 0]
        });
        const { navigation } = this.props;
        const { routes } = navigation.state;
        console.log('TABBAR RENDER');

        return (
            <Animated.View style={[styles.tabContainer, { transform: [{ translateY: bottomPosition }] }]}>
                <View style={styles.tabBarStyle} />
                <View style={styles.homeButtonShadowContainer}>
                    <View style={styles.homeButtonShadow} />
                </View>
                <View style={styles.tabButtonsContainer}>
                    {routes.map((route, index) => {
                        const customTabProps = {
                            route: route,
                            index: index,
                            navigation: navigation,
                            key: route.key,
                            icon: tabsDef[index].font,
                            title: tabsDef[index].routeName
                        };

                        return <TabButton key={`tab_${index}`} {...customTabProps} />;
                    })}
                </View>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    tabButtonsContainer: {
        width: '100%',
        height: '100%',
        flexDirection: 'row',
        position: 'absolute',
        bottom: 0,
        left: 0,
        zIndex: 10
    },
    homeButtonShadowContainer: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        bottom: 15,
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 8,
        backgroundColor: COLORS.transparent
    },
    homeButtonShadow: {
        width: 70,
        height: 70,
        borderRadius: 50,
        backgroundColor: 'green',
        shadowColor: '#9B9B9B',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 2
    },
    tabBarStyle: {
        zIndex: 9,
        position: 'absolute',
        width: '100%',
        flexDirection: 'row',
        backgroundColor: COLORS.white,
        bottom: 0,
        shadowColor: '#9B9B9B',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        ...ifIphoneX(
            {
                height: 87
            },
            {
                height: 53
            }
        )
    },
    tabContainer: {
        zIndex: 100,
        flexDirection: 'row',
        position: 'absolute',
        height: 81,
        paddingTop: 0,
        bottom: 0,
        width: '100%',
        ...ifIphoneX(
            {
                paddingTop: 0
            },
            {
                paddingTop: 31
            }
        )
    }
});

decorate(TabBar, {
    bottomAnimation: observable,
    finished: observable
});

export { TabBar };
