import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import FontAwesome from 'react-native-fontawesome';

import { COLORS, SIZES } from '../lib/conf';

type Props = {};
class TabButton extends Component<Props> {
    static propTypes = {
        navigation: PropTypes.object,
        route: PropTypes.object,
        title: PropTypes.string,
        icon: PropTypes.string,
        focused: PropTypes.bool
    };

    switchTab = () => {
        console.log('NAVIGATE', this.props.route);

        this.navigation.navigate(this.props.title);
    };

    render() {
        this.navigation = this.props.navigation;
        this.route = this.props.route;

        const { index } = this.props;
        const focused = this.navigation.state.index == index;
        const color = focused ? COLORS.teal : COLORS.gray;

        let button = null;
        let view = null;
        if (this.route.routeName === 'Home') {
            view = (
                <View style={[styles.homeButton2, Platform.OS === 'android' ? styles.homeButtonAndroid : '']}>
                    <Text style={{ color: color, fontSize: 60, fontFamily: 'uni' }}>{this.props.icon}</Text>
                </View>
            );
        } else {
            view = (
                <View style={styles.tabButton}>
                    <Text style={[styles.goin, { color: color }]}>
                        <FontAwesome>{this.props.icon}</FontAwesome>
                    </Text>
                    <Text style={[styles.tabLabel, { color: color }]}>{this.props.title}</Text>
                </View>
            );
        }
        //<Image source={focused ? tabsDef[index].iconActive : tabsDef[index].iconInactive} style={styles.tabIcon} resizeMode="contain" />
        button = <TouchableWithoutFeedback onPress={this.switchTab}>{view}</TouchableWithoutFeedback>;

        return button;
    }
}

const styles = StyleSheet.create({
    tabButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 2,
        ...ifIphoneX(
            {
                paddingBottom: 34
            },
            {
                paddingBottom: 0
            }
        )
    },
    tabLabel: {
        fontSize: SIZES.tabLabelFontSize,
        backgroundColor: 'transparent'
    },
    tabIcon: {
        width: 22,
        height: 22,
        marginBottom: 3
    },
    homeButton: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 2,
        marginTop: 2,
        marginBottom: 2,
        backgroundColor: 'green',
        // borderRadius: 32,
        width: 64
    },
    homeButtonAndroid: {
        zIndex: 100
    },
    homeButton2: {
        height: 70,
        width: 70,
        backgroundColor: COLORS.white,
        borderRadius: 50,
        borderWidth: 5,
        borderColor: COLORS.white,
        alignItems: 'center',
        justifyContent: 'center',
        top: -25
    },
    homeButtonText: {
        color: '#fff',
        fontFamily: 'goin',
        fontSize: 35
    },
    goin: {
        fontSize: SIZES.tabIconFontSize,
        marginBottom: 2,
        backgroundColor: COLORS.transparent
    }
});

export { TabButton };
