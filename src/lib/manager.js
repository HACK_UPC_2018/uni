import userStore from '../stores/UserStore';

class Manager {
    login() {}

    /* checkAccessToken() {
        if (moment())
    } */

    async getNotes(callback) {
        console.log("LET'S LOAD NOTES");
        const response = await fetch('https://api.fib.upc.edu/v2/jo/avisos/?format=json', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${userStore.accessToken}`
            }
        });
        const json = await response.json();
        console.log('GET NOTES', json);

        callback && callback(json.results);
    }
    async getHorari(callback) {
        console.log("LET'S LOAD Horari");
        const response = await fetch('https://api.fib.upc.edu/v2/jo/classes/?format=json', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${userStore.accessToken}`
            }
        });
        const json = await response.json();
        console.log('GET HORARI', json);

        callback && callback(json.results);
    }
    async getMe(callback) {
        console.log("LET'S LOAD ME");
        const response = await fetch('https://api.fib.upc.edu/v2/jo/?format=json', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${userStore.accessToken}`
            }
        });
        const json = await response.json();
        console.log('GET ME', json);

        callback && callback(json);
    }
}

const manager = new Manager();

export default manager;
