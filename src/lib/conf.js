import React from 'react';
import { Text } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { HomeScreen } from '../screens/tab-screens/HomeScreen';
import { NotesScreen } from '../screens/tab-screens/NotesScreen';
import { ScheduleScreen } from '../screens/tab-screens/ScheduleScreen';
import { ProfileScreen } from '../screens/tab-screens/ProfileScreen';
import { SettingsScreen } from '../screens/tab-screens/SettingsScreen';

const tabsDef = [
    {
        route: 'notes',
        routeName: 'Notes',
        font: Icons.stickyNote,
        screen: NotesScreen
    },
    {
        route: 'schedule',
        routeName: 'Schedule',
        font: Icons.listAlt,
        screen: ScheduleScreen
    },
    {
        route: 'home',
        routeName: 'Home',
        font: '\ue900',
        screen: HomeScreen
    },
    {
        route: 'profile',
        routeName: 'Profile',
        font: Icons.user,
        screen: ProfileScreen
    },
    {
        route: 'services',
        routeName: 'Services',
        font: Icons.bars,
        screen: SettingsScreen
    }
];

const COLORS = {
    transparent: '#00000000',
    white: '#ffffff',
    teal: '#007bc0',
    gray: '#8c8c8c'
};
const SIZES = {
    tabIconFontSize: 22,
    tabLabelFontSize: 11
};

export { tabsDef, COLORS, SIZES };
