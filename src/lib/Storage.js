import { AsyncStorage } from 'react-native';
import SecureInfo from 'react-native-sensitive-info';

class LocalStore {
    constructor() {}

    async getValue(key, callback) {
        try {
            await AsyncStorage.getItem(key, callback);
        } catch (error) {
            //Error catch
        }
    }

    async setValue(key, value, callback) {
        try {
            await AsyncStorage.setItem(key, value, callback);
        } catch (error) {
            //Error catch
        }
    }

    async deleteValue(key, callback) {
        try {
            await AsyncStorage.removeItem(key, callback);
        } catch (error) {
            //Error catch
        }
    }
}

class LocalSecureStore {
    constructor() {
        this.cache = {};
    }

    get = async key => {
        try {
            if (this.cache[key]) {
                return this.cache[key];
            }

            const getResult = (await SecureInfo.getItem(key, {})) || null;
            this.cache[key] = JSON.parse(getResult);

            return this.cache[key];
        } catch (error) {
            return null;
        }
    };

    set = async (key, value) => {
        try {
            await SecureInfo.setItem(key, JSON.stringify(value), {});
            this.cache[key] = value;
            return this.cache[key];
        } catch (error) {
            return null;
        }
    };

    delete = key => {
        SecureInfo.deleteItem(key, {});
    };
}

export { LocalStore, LocalSecureStore };
