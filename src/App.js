/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TabNavigator, StackNavigator, BottomTabBar, createBottomTabNavigator } from 'react-navigation';
import _ from 'lodash';
import FontAwesome, { Icons } from 'react-native-fontawesome';

import { tabsDef, COLORS } from './lib/conf';
import userStore from './stores/UserStore';
import { navStore } from './stores/NavigationStore';
import { TabBar } from './components/TabBar';
import LoginScreen from './screens/auth/LoginScreen';
import WelcomeScreen from './screens/auth/WelcomeScreen';

const tabScreens = _.keyBy(tabsDef, 'routeName');

const tabNavigationOptions = {
    navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
            const { routeName } = navigation.state;
            const route = tabScreens[routeName];

            if (routeName === 'Home') {
                return (
                    <View
                        style={{
                            position: 'relative',
                            alignItems: 'center',
                            justifyContent: 'center',
                            top: -20,
                            width: 60,
                            height: 60,
                            borderRadius: 30,
                            backgroundColor: COLORS.white,
                            shadowColor: COLORS.gray,
                            shadowOpacity: 0.5,
                            shadowRadius: 3,
                            shadowOffset: { x: 0, y: 0 }
                        }}
                    >
                        <Text style={{ color: tintColor, fontSize: 60, fontFamily: 'uni' }}>{route.font}</Text>
                    </View>
                );
            }
            return (
                <Text style={{ color: tintColor, fontSize: 25, top: 3 }}>
                    <FontAwesome>{route.font}</FontAwesome>
                </Text>
            );
        },
        tabBarLabel: ({ tintColor }) => {
            const { routeName } = navigation.state;
            const route = tabScreens[routeName];

            // You can return any component that you like here! We usually use an
            // icon component from react-native-vector-icons
            console.log('ROUTE ICON', route);
            if (routeName !== 'Home') {
                return (
                    <Text
                        style={{
                            fontSize: 11,
                            marginBottom: 1.5,
                            color: tintColor,
                            backgroundColor: COLORS.transparent,
                            textAlign: 'center'
                        }}
                    >
                        {routeName}
                    </Text>
                );
            } else {
                return null;
            }
        }
    }),
    initialRouteName: 'Home',
    tabBarComponent: BottomTabBar,
    tabBarPosition: 'bottom',
    tabBarOptions: {
        activeTintColor: COLORS.teal,
        inactiveTintColor: 'gray'
    },
    animationEnabled: true,
    swipeEnabled: true
};

const TabNavigation = TabNavigator(tabScreens, {
    initialRouteName: 'Home',
    tabBarPosition: 'bottom',
    tabBarComponent: TabBar,
    lazy: false,
    animationEnabled: true,
    swipeEnabled: true,
    backBehavior: 'initialRoute'
});

const RootNavigation = StackNavigator(
    {
        welcome: {
            screen: WelcomeScreen
        },
        login: {
            screen: LoginScreen
        },
        tabScreens: {
            screen: TabNavigation
        }
    },
    {
        headerMode: 'none',
        mode: 'card',
        navigationOptions: {
            headerStyle: {
                backgroundColor: 'white'
            }
        }
    }
);

type Props = {};
export default class App extends Component<Props> {
    render() {
        if (!userStore.ready) {
            return null;
        }
        console.log('STORE USER', userStore);

        /* if (userStore.accessToken === '') {
            return <LoginScreen />;
        } else { */
        return <RootNavigation />;
        /* } */
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5
    },
    demo: {
        backgroundColor: 'yellow',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    demo1: {
        flex: 1,
        width: '100%',
        backgroundColor: 'green',
        alignItems: 'center',
        justifyContent: 'center'
    },
    demo2: {
        flex: 1,
        width: '100%',
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
