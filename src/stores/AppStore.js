/* import { AsyncStorage, NetInfo } from 'react-native';
import { decorate, observable } from 'mobx';
import { persist, create } from 'mobx-persist';

const hydrate = create({ storage: AsyncStorage });

class AppStore {
    notes = [];
    schedule = [];
    ready = false;
}

const schema = {
    notes: {
        type: 'list'
    },
    schedule: {
        type: 'list'
    }
};

const store = new AppStore();

export const someStore = persist(schema)(store);

decorate(store, {
    notes: observable,
    ready: observable,
    schedule: observable
})(async () => {
    await Promise.all(
        ['notes', 'schedule'].map(async key => {
            return hydrate(key, store);
        })
    );
    store.ready = true;
})();

export default store;
 */
