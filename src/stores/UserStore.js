import { action, observable, decorate } from 'mobx';
import { LocalSecureStore } from '../lib/Storage';

const localStorage = new LocalSecureStore();

class UserStore {
    _accessToken;
    _renewToken;
    _accessTokenExpiry;
    _renewTokenExpiry;
    ready = false;

    constructor() {
        (async () => {
            this._accessToken = (await localStorage.get('accessToken')) || '';
            this._accessTokenExpiry = (await localStorage.get('accessTokenExpiry')) || 0;
            this._renewToken = (await localStorage.get('renewToken')) || '';
            this._renewTokenExpiry = (await localStorage.get('renewTokenExpiry')) || 0;
            this.ready = true;
        })();
    }

    get accessToken() {
        return this._accessToken;
    }

    get renewToken() {
        return this._renewToken;
    }

    get accessTokenExpiry() {
        return this._accessTokenExpiry;
    }

    get renewTokenExpiry() {
        return this._renewTokenExpiry;
    }

    set accessToken(token) {
        localStorage.set('accessToken', token);
        this._accessToken = token;
    }

    set renewToken(token) {
        localStorage.set('renewToken', token);
        this._renewToken = token;
    }

    set accessTokenExpiry(expiry) {
        localStorage.set('accessTokenExpiry', expiry);
        this._accessTokenExpiry = expiry;
    }

    set renewTokenExpiry(expiry) {
        localStorage.set('renewTokenExpiry', expiry);
        this._renewTokenExpiry = expiry;
    }

    delete(key) {
        localStorage.delete(key);
    }

    deleteAll() {
        this.delete('accessToken');
        this.delete('accessTokenExpiry');
        this.delete('renewToken');
        this.delete('renewTokenExpiry');

        this._accessToken = '';
        this._renewToken = '';
        this._accessTokenExpiry = 0;
        this._renewTokenExpiry = 0;

        this.ready = true;
    }
}

const store = new UserStore();

decorate(store, {
    _accessToken: observable,
    _renewToken: observable,
    _accessTokenExpiry: observable,
    _renewTokenExpiry: observable,
    ready: observable,
    delete: action,
    deleteAll: action
});

export default store;
