import { observable, action, decorate } from 'mobx';
import { NavigationActions } from 'react-navigation';

class RootNavigationStore {
    rootNavigator;
    ready = false;

    navigationState = {
        index: 0,
        routes: []
    };

    initRootNavigator = (navigator, initialRouteName) => {
        this.rootNavigator = navigator;
        this.navigationState.routes = [{ key: initialRouteName, routeName: initialRouteName }];
        this.ready = true;
    };

    resetQueue = routeName => {
        this.navigationState = {
            index: 0,
            routes: [{ key: routeName, routeName: routeName }]
        };
    };

    reset() {
        this.ready = false;
        this.navigationState = {
            index: 0,
            routes: []
        };
    }

    goBack = () => {
        this.dispatchNavigation(NavigationActions.back({ key: null }));
    };

    navigate = (screen, stackNavState = false, params = null) => {
        this.dispatchNavigation(NavigationActions.navigate({ routeName: screen, params: params }), stackNavState);
    };

    get currentRouteName() {
        return this.navigationState.routes[this.navigationState.index].routeName;
    }

    get routeParams() {
        return this.navigationState.routes[this.navigationState.index].params;
    }

    // NOTE: the second param, is to avoid stacking and reset the nav state
    dispatchNavigation = (action, stackNavState = true) => {
        const previousNavState = stackNavState ? this.navigationState : null;
        this.navigationState = this.rootNavigator.router.getStateForAction(action, previousNavState);
        return this.navigationState;
    };
}

const navStore = new RootNavigationStore();

decorate(navStore, {
    rootNavigator: observable,
    ready: observable,
    navigationState: observable,
    initRootNavigator: action,
    resetQueue: action,
    reset: action,
    goBack: action,
    navigate: action,
    dispatchNavigation: action
});

export { navStore };
